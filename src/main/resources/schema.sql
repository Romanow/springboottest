create TABLE mytable (
    id integer not null,
    text varchar(80) not null,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX mytable_pkey ON mytable (id);
CREATE UNIQUE INDEX unique_id ON mytable (id);
create TABLE test_class (
    id integer not null default nextval('test_class_id_seq'::regclass),
    "count" integer,
    text varchar(80),
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX test_class_pkey ON test_class (id);
create TABLE test_data (
    id integer not null default nextval('test_data_id_seq'::regclass),
    text varchar(255),
    test_class integer,
    PRIMARY KEY (id),
    FOREIGN KEY (test_class) REFERENCES test_class (id)
);
CREATE UNIQUE INDEX test_data_pkey ON test_data (id);