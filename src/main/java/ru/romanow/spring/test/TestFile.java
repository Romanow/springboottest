package ru.romanow.spring.test;

/**
 * Created by ronin on 12.11.15
 */

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Change class signature Ctrl + F6
 * Change class name Shift + F6
 * Extract super class
 * Extract Interface
 *
 * User Interface where possible
 * Replace contructor with builder
 * Replace Inheritance with delegation
 *
 * Smart code completion Ctrl + Shift + Space
 * Chain completion Smart completion twice
 * Complete statement Ctrl + Shift + Enter
 * Parameter info Ctrl + P
 */
public class TestFile {
    private static final Logger logger = LoggerFactory.getLogger(TestFile.class);

    // Parameter object
    // Wrap return value
    // Type migration
    public String test(final String param1, final String param2,
                               final String param3, final String param4) {
        // Extract variable
        // Extract method
        String res1 = param1 + getLengthAsString(param2);
        String res2 = param3 + getLengthAsString(param4);

        logger.info("{}", testNullable().length());
        logger.info("{}", testNotNull() != null ? testNotNull().length() : "");

        // Inspection -> Internalization Issues -> Hard code strings
        testNls("Привет");
        testNonNls("Привет");

        return res1 + res2;
    }

    private @Nullable String getLengthAsString(@Nullable String str) {
        return str != null ? String.valueOf(str.length()) : null;
    }

    private @Nullable String testNullable() {
        return null;
    }

    private @NotNull String testNotNull() {
        return "";
    }

    private void testNls(@Nls String str) {
        logger.debug("{}", str);
    }

    private void testNonNls(@NonNls String str) {
        logger.debug("{}", str);
    }
}
