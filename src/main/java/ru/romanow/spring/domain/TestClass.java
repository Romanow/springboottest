package ru.romanow.spring.domain;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ronin on 12.11.15
 */
@Entity
@Table(name = "test_class")
public class TestClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "text", length = 80)
    private String text;

    @Column(name = "count")
    private Integer count;

    @OneToMany(mappedBy = "testClass")
    private List<TestData> dataList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<TestData> getDataList() {
        return dataList;
    }

    public void setDataList(List<TestData> dataList) {
        this.dataList = dataList;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("id", id)
                          .add("text", text)
                          .add("count", count)
                          .toString();
    }
}
